package hu.novaservices;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {


    static Map<String, List<String>> questionsAndAnswers = new HashMap<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        play();
    }

    public static void play(){
        System.out.println(
                        "Press 1, if you want to ask a specific question.\n" +
                        "Press 2, if you want to add a question with answers.\n" +
                        "Press 3, if you want to exit.\n" +
                        "Press a button:\n" );

        String answer = scanner.next();
        switch (answer){
            case "1":
                askQuestion();
                break;
            case "2":
                addQuestion();
                break;
            case "3":
                return;
            default:
                System.out.println("You pressed wrong button!\n");
                play();
        }
    }

    public static void askQuestion(){
        System.out.println("Ask your question!\n");
        scanner.nextLine();
        String question = scanner.nextLine();
        questionsAndAnswers.forEach((quest, answers) -> {
            if(quest.equals(question)){
                writeAnswers(answers);
                play();
            }
        });
        write42();
        play();
    }

    public static void addQuestion(){
        System.out.println("Add your question and the answers\n");
        scanner.nextLine();
        String questionAndAnswers = scanner.nextLine();
        String[] parts = questionAndAnswers.split("\\?");
        String question = parts[0] + "?";
        if(!isLengthGood(question)){
            System.out.println("The question is too long. Give me a shorter one!\n");
            addQuestion();
        }
        if (!checkQuestionExist(question)) {
            String answers = parts[1];
            List<String> answerList = getAnswerList(answers);
            if(!isAnswerLengthGood(answerList)){
                System.out.println("One of the answer is too long. Give me a shorter one!\n");
                addQuestion();
            }
            questionsAndAnswers.put(question, answerList);
            System.out.println("You added the question with the answers!\n");
        }
        play();
    }

    public static List<String> getAnswerList(String answers){
        List<String> answerList = new ArrayList<>();
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(answers);
        while (m.find()) {
            answerList.add(m.group(1));
        }
        return answerList;
    }

    public static void writeAnswers(List<String> answers){
        for(String s : answers){
            System.out.println(s + "\n");
        }
    }

    public static void write42(){
        System.out.println("The answer to life, universe and everything is 42.\n");
    }

    public static boolean isLengthGood(String text){
        return text.length() < 256;
    }

    public static boolean isAnswerLengthGood(List<String> answers){
        boolean isLengthGood = true;
        for(String answer: answers){
            if(!isLengthGood(answer)){
                isLengthGood = false;
            }
        }
        return isLengthGood;
    }

    public static boolean checkQuestionExist(String question){
        for(Map.Entry<String, List<String>> entry : questionsAndAnswers.entrySet()){
            if(entry.getKey().equals(question)){
                writeAnswers(entry.getValue());
                return true;
            }
        }
        return false;
    }
}
